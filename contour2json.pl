#!/usr/bin/perl

use strict;

my ($activeinsulin, $sensormg, $sensormol, $sensordate, $bgltrend, $currentbasal, $tempbasalrate, $tempbasalpercent, $unitsremain, $batteryremain, $pumpstart, $pumpend, $pumpsize);
my $data;
open(INPUT,"<output.txt") || die $!;
{ local $/;
  $data = <INPUT>;
}
close(INPUT);

($activeinsulin) = $data =~ /Active Insulin\: ([\d\.]*)U/i;
($sensormg,$sensormol,$sensordate) = $data =~ /Sensor BGL\: ([\d\.]*) mg\/dL \(([\d\.]*) mmol\/L\) at ([^\n]*)/i;
($bgltrend) = $data =~ /BGL trend\: ([^\n]*)/i;
($currentbasal) = $data =~ /Current basal rate\: ([\d\.]*)U/i;
($tempbasalrate) = $data =~ /Temp basal rate\: ([\d\.]*)U/i;
($tempbasalpercent) = $data =~ /Temp basal percentage\: ([\d\.]*)\%/i;
($unitsremain) = $data =~ /Units remaining\: ([\d\.]*)U/i;
($batteryremain) = $data =~ /Battery remaining\: ([\d\.]*\%)/i;
($pumpstart) = $data =~ /Pump Start\: ([^\n]*)/i;
($pumpend) = $data =~ /Pump End\: ([^\n]*)/i;
($pumpsize) = $data =~ /Pump Size\: ([^\n]*)/i;

print <<JSON;
{
\t"status": {
\t\t"ActiveInsulin": "$activeinsulin",
\t\t"SensorBGLmg": "$sensormg",
\t\t"SensorBGLmmol": "$sensormol",
\t\t"SensorDate": "$sensordate",
\t\t"BGLtrend": "$bgltrend",
\t\t"Currentbasalrate": "$currentbasal",
\t\t"Tempbasalrate": "$tempbasalrate",
\t\t"Tempbasalpercentage": "$tempbasalpercent",
\t\t"Unitsremaining": "$unitsremain",
\t\t"Batteryremaining": "$batteryremain",
\t\t"Pumphistoryinfo": {
\t\t\t"PumpStart": "$pumpstart",
\t\t\t"PumpEnd": "$pumpend",
\t\t\t"PumpSize": "$pumpsize"
\t\t\t}
\t\t}
}
JSON
