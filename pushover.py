import http.client, urllib
import sys

# Change lines below!
# Look up this information on https://pushover.net
app_token = "<APP TOKEN HERE!!>"
user_key = "<USER KEY HERE!!>"
# End Changes

message = sys.argv[1];

conn = http.client.HTTPSConnection("api.pushover.net:443")
conn.request("POST", "/1/messages.json",
  urllib.parse.urlencode({
    "token": app_token,
    "user": user_key,
    "message": message,
  }), { "Content-type": "application/x-www-form-urlencoded" })
conn.getresponse()
