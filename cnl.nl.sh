#!/bin/bash

# Main loop script for the CNL24 project
# https://gitlab.com/hendrikvb/cnl24pi
# Use Contour Next Link 2.4 Blood Glucose Meter to retrieve readouts from 
# a Minimed 640g series Insuline Pump
# Last Modified: 17 December 2020

export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

bottomalarm=79
# Set this to bottom alarm threshold

topalarm=300
# Set this to top alarm threshold

cnlusb=/dev/hidraw0
# Set this to the USB device that is mounted when you connect the CNL 2.4

ghvolume=65
# Set Google Home Volume here

amixer set PCM 100%
# Set speaker volume here

normalinterval=150
# Normal wait period is 2.5 minutes (150 seconds)

shortinterval=90
# Short interval is 1.5 minutes (90 seconds)

alarmlow=alarm.mp3
alarmhigh=alarmhigh.mp3
# Tweak MP3 files here for alarms
# Change lines below to play MP3s locally or through Google Home
# Use ghplay.py for Google Home with MP3s
# Use ghsay.py for Google Home with Voice Notification

googlehome=192.168.2.111
googlehomenight=192.168.2.165
#googlehome=
#googlehomenight=
# Only needed when Google Home is used.
# Set value to IP for Google Home device.
# Use blank value to disable Google Home integration
# Define googlehomenight to use MP3 alarm at night
# Primary GH device is still used for standard notification (by default script behaviour)
# Set googlehomenight to same IP in case you want night-mode behaviour on primary device (audio warnings over GH device) 

nighthour=20
morninghour=12
# Set window for *night mode* - Hours only - 24h format
# Example values:
# nighthour=20
# morninghour=7
# This defines a window: 20:00 to 7:59 for which an additional Google Home device is used

language=nl

# Customize where needed above

norepeat=0
# Flag used to prevent repetive notifications. Ignore
# Possible values tested:
# 1: No sensor connection
# 2: Sensor reading excessive (calibration)
# 3: No reading / zero values
# 10: BGL too high
# 11: BGL too low
# 12: Sensor initializing
# 13: Calibration needed
# 14: Sensor or calibration error
# 15: Sensor end of life
# 16: Sensor calibration pending
# 20: Custom bracket notifications

noflap=0
# Flag used to prevent flapping

zeroflag=1
# Counter to trigger action after maxzero counts
maxzero=3
# Execute zerocommand after maxzero counts
zerocommand=/sbin/reboot

echo Press Ctrl-C to break
for (( ; ; ))
do
   date=`date`
   chour=`date +"%H"`
   if [ $zeroflag == $maxzero ]; then
     echo "Repetitive ZERO levels. Attempting to remediate" | tee -a output.log
     echo "Repetitive ZERO levels. Attempting to remediate - $date" > status.log
     `$zerocommand`
   fi
   
   if [ ! -e "$cnlusb" ]; then
     echo "No CNL Connected - $date" | tee -a output.log
     ((zeroflag=zeroflag+1))
     sleep $normalinterval 
     continue;
   fi
   python -m decoding-contour-next-link.read_minimed_next24 > output.txt 2> /dev/null
   perl contour2json.pl > cnl.json
   bgl=`cat cnl.json | jq '.status.SensorBGLmg' | sed s/\"//g`
   units=`cat cnl.json | jq '.status.Unitsremaining' | sed s/\"//g`
   active=`cat cnl.json | jq '.status.ActiveInsulin' | sed s/\"//g`
   if [[ $bgl -eq 0 && `printf "%.0f" $units` -ne 0 ]]; then
     echo "No sensor connection with pump!" | tee -a output.log
     echo "No sensor connection with pump! - $date" > status.log
     if [[ $norepeat -ne 1 ]]; then
         if [[ ! -z $googlehome ]]; then
            echo
            #python3 ghsay.py $googlehome "Geen sensor verbinding met pomp!" $language $ghvolume
         fi
     fi
     norepeat=1
     sleep $normalinterval
     continue
   fi;
   
   if [ ! -n "$bgl" ]; then 
     bgl=0
     echo Oops
   fi;

   if [ $bgl == 0 ]; then
     echo "Empty Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Empty Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
          echo ""
           # python3 ghsay.py $googlehome "Sensor resultaat is leeg." $language $ghvolume
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 769 ]; then
     echo "Initializing sensor" | tee -a output.log
     echo "Initializing sensor - $date" > status.log
     if [[ $norepeat -ne 12 ]]; then
         if [[ ! -z $googlehome ]]; then
          echo ""
           # python3 ghsay.py $googlehome "Sensor resultaat is leeg." $language $ghvolume
         fi
     fi
     norepeat=12
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 770 ]; then
     echo "Calibration needed - Active Insulin: $active" | tee -a output.log
     echo "Calibration needed - Active Insulin: $active - $date" > status.log
     if [[ $norepeat -ne 13 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Sensorcalibratie nodig. Actieve Insuline is $active" $language $ghvolume
          python3 pushover.py "Sensorcalibratie nodig. Actieve Insuline is $active" 
         fi
     fi
     norepeat=13
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 771 ]; then
     echo "Sensor error" | tee -a output.log
     echo "Sensor error - $date" > status.log
     if [[ $norepeat -ne 14 ]]; then
         if [[ ! -z $googlehome ]]; then
          echo ""
          # python3 ghsay.py $googlehome "Sensor Fout" $language $ghvolume
          # python3 pushover.py "Sensor Fout"  
         fi
     fi
     norepeat=14
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 772 ]; then
     echo "Calibration error" | tee -a output.log
     echo "Calibration error - $date" > status.log
     if [[ $norepeat -ne 14 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Fout bij sensorcalibratie." $language $ghvolume
          python3 pushover.py "Fout bij sensorcalibratie." 
         fi
     fi
     norepeat=14
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 774 ]; then
     echo "Sensor end of life" | tee -a output.log
     echo "Sensor end of life - $date" > status.log
     if [[ $norepeat -ne 15 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Vervang sensor. Sensor opgebruikt." $language $ghvolume
          python3 pushover.py "Vervang sensor. Sensor opgebruikt." 
         fi
     fi
     norepeat=15
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 775 ]; then
     echo "Sensor not ready" | tee -a output.log
     echo "Sensor not ready - $date" > status.log
     if [[ $norepeat -ne 14 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Sensor niet beschikbaar." $language $ghvolume
          python3 pushover.py "Sensor niet beschikbaar." 
         fi
     fi
     norepeat=14
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 776 ]; then
     echo "Sensor value above 400!" | tee -a output.log
     echo "Sensor value above 400! - $date" > status.log
     #if [[ $norepeat -ne 10 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Sensorwaarde boven 400!" $language $ghvolume
          python3 pushover.py "Sensorwaarde boven 400!" 
         fi
     #fi
     norepeat=10
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 777 ]; then
     echo "Sensor value too low!" | tee -a output.log
     echo "Sensor value too low! - $date" > status.log
     #if [[ $norepeat -ne 11 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Sensorwaarde veel te laag!" $language $ghvolume
          python3 pushover.py "Sensorwaarde veel te laag!" 
         fi
     #fi
     norepeat=11
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl == 778 ]; then
     echo "Sensor calibration pending." | tee -a output.log
     echo "Sensor calibration pending - $date" > status.log
     if [[ $norepeat -ne 16 ]]; then
         if [[ ! -z $googlehome ]]; then
          #echo ""
          python3 ghsay.py $googlehome "Sensorcalibratie in verwerking" $language $ghvolume
          python3 pushover.py "Sensorcalibratie in verwerking." 
         fi
     fi
     norepeat=16
     sleep $shortinterval 
     continue;
   fi;

   python jsontodb.py
   python dbtojson.py
   
   if [ -z $bgl ]; then
     echo "Zero Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Zero Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
            # python3 ghsay.py $googlehome "Sensor resultaat heeft nul waarde." $language $ghvolume
            echo ""
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     continue;
   fi;
   
   if [ $bgl == 0 ]; then
     echo "Empty Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Empty Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
          echo ""
           # python3 ghsay.py $googlehome "Sensor resultaat is leeg." $language $ghvolume
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     continue;
   fi;

   if [ $bgl -lt $bottomalarm ]; then
     echo "ALARM! BGL TOO LOW! - $bgl - $date" | tee -a output.log
     if [[ $norepeat -ne 10 ]]; then
     	mpg123 $alarmlow &
        if [[ ! -z $googlehome ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            python3 ghsay.py $googlehome "Bloed Glucose Niveau is te laag! Waarde is $bgl." $language $ghvolume
            python3 pushover.py "Bloed Glucose Niveau is te laag! Waarde is $bgl."
            # python3 ghplay.py $googlehome $alarmlow
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
         if [[ ! -z $googlehomenight && ($chour -ge $nighthour || $chour -le $morninghour) ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            # python3 ghsay.py $googlehomenight "Bloed Glucose Niveau is te laag! Waarde is $bgl." $language $ghvolume
            # python3 pushover.py "Bloed Glucose Niveau is te laag! Waarde is $bgl."
            python3 ghplay.py $googlehomenight $alarmlow
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
         fi

        echo "Blood Glucose Level too low! - $date" > status.log
        norepeat=10
      fi
      norepeat=10
      sleep $normalinterval
      continue;
   fi;
   
   if [ $bgl -gt $topalarm ]; then
     echo "ALARM! BGL TOO HIGH! - $bgl - $date" | tee -a output.log
     if [[ $norepeat -ne 11 ]]; then
        # mpg123 $alarmhigh &
        if [[ ! -z $googlehome ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            python3 ghsay.py $googlehome "Bloed Glucose Niveau is te hoog! Waarde is $bgl." $language $ghvolume
            python3 pushover.py "Bloed Glucose Niveau is te hoog! Waarde is $bgl."
            # python3 ghplay.py $googlehome $alarmhigh
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        if [[ ! -z $googlehomenight && ($chour -ge $nighthour || $chour -le $morninghour) ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            # python3 ghsay.py $googlehomenight "Bloed Glucose Niveau is te hoog! Waarde is $bgl." $language $ghvolume
            # python3 pushover.py "Bloed Glucose Niveau is te hoog! Waarde is $bgl."
            python3 ghplay.py $googlehomenight $alarmhigh
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi

        echo "Blood Glucose Level too high! - $date" > status.log
     fi
     norepeat=11
     sleep $normalinterval
     continue;
   fi;

   echo "No Alarm - $bgl - $date" | tee -a output.log
   echo "OK - $date" > status.log
   
   # Sample bracket notifications below - Customize where needed

   # 100 - 150
   if [[ $bgl -gt 100 && $bgl -lt 150 && $norepeat -ne 20 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Glucose waarde is $bgl." $language $ghvolume
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=20
        sleep $normalinterval 
        continue;
   fi;

   # 150 - 200
   if [[ $bgl -gt 150 && $bgl -lt 200 && $norepeat -ne 21 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Glucose waarde is $bgl." $language $ghvolume
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=21
        sleep $normalinterval 
        continue;
   fi;

   # 200 - 250
   if [[ $bgl -gt 200 && $bgl -lt 250 && $norepeat -ne 22 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Glucose waarde is $bgl." $language $ghvolume
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=22
        sleep $normalinterval 
        continue;
   fi;

   # 250 - 300
   if [[ $bgl -gt 250 && $bgl -lt 300 && $norepeat -ne 23 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Glucose waarde is $bgl." $language $ghvolume
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=23
        sleep $normalinterval 
        continue;
   fi;

   sleep $normalinterval
done
