#! /usr/bin/python3.5

# Mostly based on code listed on
# https://www.gioexperience.com/google-home-hack-send-voice-programmaticaly-with-python/
# Usage:
# python3 ghplay.py 192.168.1.1 alarm.mp3 <volume optional>
# Provide Google Home / Chromecast as first parameter
# Provide MP3 file as second parameter
# Provide optional speaker volume (1-100) as third parameter

# Uses Python 3
# Install dependencies:
# pip3 install pychromecast
# pip3 install gtts

import sys
import pychromecast
import os
import os.path
from gtts import gTTS
import time
import hashlib

ip=sys.argv[1];
fname=sys.argv[2];

#********* retrieve local ip of my rpi3
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
local_ip=s.getsockname()[0]
s.close()
#**********************

vol=-1;

try:
   vol=sys.argv[3]; #from 0 to 100
except:
   pass

castdevice = pychromecast.Chromecast(ip)
castdevice.wait()

# Store current volume
vol_previous=castdevice.status.volume_level
vol_prec=0.0
if vol==-1 :
   vol_prec=castdevice.status.volume_level
else :
   vol_prec=float(vol)/100

castdevice = pychromecast.Chromecast(ip)
castdevice.wait()
castdevice.set_volume(0.0) #set volume 0 for not hear the BEEEP

mc = castdevice.media_controller
# Update HTTP Server Port if needed
mc.play_media("http://"+local_ip+":8080/"+fname, "audio/mp3")

mc.block_until_active()

mc.pause() #prepare audio and pause...

time.sleep(1)
castdevice.set_volume(vol_prec) #setting volume to precedent value
time.sleep(0.2)

mc.play() #play the mp3

while not mc.status.player_is_idle:
   time.sleep(0.5)

mc.stop()
# Set original volume back
castdevice.set_volume(vol_previous)

castdevice.quit_app()