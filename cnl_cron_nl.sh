#!/bin/bash

# Cron script for the CNL24 project
# https://gitlab.com/hendrikvb/cnl24pi
# Use Contour Next Link 2.4 Blood Glucose Meter to retrieve readouts from 
# a Minimed 640g series Insuline Pump
# Last Modified: 23 December 2020

export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

bottomalarm=79
# Set this to bottom alarm threshold

topalarm=300
# Set this to top alarm threshold

ghvolume=65
# Set Google Home Volume here

cnlusb=/dev/hidraw0
# Set this to the USB device that is mounted when you connect the CNL 2.4

googlehome=192.168.2.111
googlehomenight=192.168.2.165
#googlehome=
#googlehomenight=
# Only needed when Google Home is used.
# Set value to IP for Google Home device.
# Use blank value to disable Google Home integration
# Define googlehomenight to use MP3 alarm at night
# Primary GH device is still used for standard notification (by default script behaviour)
# Set googlehomenight to same IP in case you want night-mode behaviour on primary device (audio warnings over GH device)

nighthour=20
morninghour=12
# Set window for *night mode* - Hours only - 24h format
# Example values:
# nighthour=20
# morninghour=7
# This defines a window: 20:00 to 7:59 for which an additional Google Home device is used

language=nl
# Customize where needed above

path=/root/cnl24pi/

cd $path

date=`date`
chour=`date +"%H"`

python -m decoding-contour-next-link.read_minimed_next24 > output.txt 2> /dev/null
perl contour2json.pl > cnl.json
bgl=`cat cnl.json | jq '.status.SensorBGLmg' | sed s/\"//g`
units=`cat cnl.json | jq '.status.Unitsremaining' | sed s/\"//g`
active=`cat cnl.json | jq '.status.ActiveInsulin' | sed s/\"//g`

if [[ $bgl -eq 0 && `printf "%.0f" $units` -ne 0 ]]; then
 echo "No sensor connection with pump! - $date" > status.log
 if [[ ! -z $googlehome ]]; then
   python3 ghsay.py $googlehome "Geen sensor verbinding met pomp!" $language $ghvolume
 fi
 exit
fi;
   
if [[ $bgl == 769 ]]; then
  echo "Initializing sensor" | tee -a output.log
  echo "Initializing sensor - $date" > status.log
  if [[ ! -z $googlehome ]]; then
     echo ""
      # python3 ghsay.py $googlehome "Sensor resultaat is leeg." $language $ghvolume
  fi
  exit
fi;

if [[ $bgl == 770 ]]; then
  echo "Calibration needed - Active Insulin: $active" | tee -a output.log
  echo "Calibration needed - Active Insulin: $active - $date" > status.log
  if [[ ! -z $googlehome ]]; then
     #echo ""
     python3 ghsay.py $googlehome "Sensorcalibratie nodig. - Actieve Insulin: $active" $language $ghvolume
     python3 pushover.py "Sensorcalibratie nodig - Actieve Insulin: $active." 
  fi
  exit
 fi;

 if [[ $bgl == 771 ]]; then
  echo "Sensor error" | tee -a output.log
  echo "Sensor error - $date" > status.log
  if [[ ! -z $googlehome ]]; then
      echo ""
      # python3 ghsay.py $googlehome "Sensor Fout" $language $ghvolume
      # python3 pushover.py "Sensor Fout"
  fi;
  exit
fi

if [ $bgl == 772 ]; then
  echo "Calibration error" | tee -a output.log
  echo "Calibration error - $date" > status.log
  if [[ ! -z $googlehome ]]; then
      #echo ""
      python3 ghsay.py $googlehome "Fout bij sensorcalibratie." $language $ghvolume
      python3 pushover.py "Fout bij sensorcalibratie."
  fi
  exit
fi;

if [[ $bgl == 774 ]]; then
  echo "Sensor end of life" | tee -a output.log
  echo "Sensor end of life - $date" > status.log
  if [[ ! -z $googlehome ]]; then
     #echo ""
     python3 ghsay.py $googlehome "Vervang sensor. Sensor opgebruikt." $language $ghvolume
     python3 pushover.py "Vervang sensor. Sensor opgebruikt." 
  fi
  exit
fi;

if [[ $bgl == 775 ]]; then
  echo "Sensor not ready" | tee -a output.log
  echo "Sensor not ready - $date" > status.log
  if [[ ! -z $googlehome ]]; then
      #echo ""
      python3 ghsay.py $googlehome "Sensor niet beschikbaar." $language $ghvolume
      python3 pushover.py "Sensor niet beschikbaar." 
  fi
  exit
fi;

if [[ $bgl == 776 ]]; then
  echo "Sensor value above 400!" | tee -a output.log
  echo "Sensor value above 400! - $date" > status.log
  if [[ ! -z $googlehome ]]; then
      #echo ""
      python3 ghsay.py $googlehome "Sensorwaarde boven 400!" $language $ghvolume
      python3 pushover.py "Sensorwaarde boven 400!"
  fi
  exit
fi;

if [[ $bgl == 777 ]]; then
  echo "Sensor value too low!" | tee -a output.log
  echo "Sensor value too low! - $date" > status.log
  if [[ ! -z $googlehome ]]; then
      #echo ""
      python3 ghsay.py $googlehome "Sensorwaarde veel te laag!" $language $ghvolume
      python3 pushover.py "Sensorwaarde veel te laag!"
  fi
  exit
fi;

if [[ $bgl == 778 ]]; then
  echo "Sensor calibration pending." | tee -a output.log
  echo "Sensor calibration pending - $date" > status.log
  if [[ ! -z $googlehome ]]; then
     #echo ""
     python3 ghsay.py $googlehome "Sensorcalibratie in verwerking" $language $ghvolume
     python3 pushover.py "Sensorcalibratie in verwerking." 
  fi
  exit
fi;
   
python jsontodb.py
python dbtojson.py
   
if [[ -z $bgl ]]; then
  echo "Zero Values - $date" > status.log
  if [[ ! -z $googlehome ]]; then
    python3 ghsay.py $googlehome "Sensor resultaat heeft nul waarde." $language $ghvolume
  fi
  exit
fi;
   
if [[ $bgl == 0 ]]; then
  echo "Empty Values - $date" > status.log
  if [[ ! -z $googlehome ]]; then
    python3 ghsay.py $googlehome "Sensor resultaat is leeg." $language $ghvolume
  fi
  exit
fi

# Normal reading
if [[ ! -z $googlehome ]]; then
  python3 ghsay.py $googlehome "Glucose waarde is $bgl" $language $ghvolume
fi
